import { useRouter } from 'next/router';
import dynamic from 'next/dynamic';
const ServiceDetails = dynamic(() => import('../components/ServiceDetails'), { ssr: false });
import { getLogger } from '@app/utils/logger';

export default function Index() {
  const logger = getLogger('index');

  const router = useRouter();
  const missingParams: string[] = [];

  if (router.query.clientId === undefined || Array.isArray(router.query.clientId)) {
    missingParams.push('clientId');
  }
  if (router.query.scope === undefined || Array.isArray(router.query.scope)) {
    missingParams.push('scope');
  }
  if (router.query.state === undefined || Array.isArray(router.query.state)) {
    missingParams.push('state');
  }

  if (missingParams.length > 0) {
    logger.info({ missingParams }, 'Missing required parameters');

    return (
      <div data-testid="paramErrorMessage">
        Error, query parameter <em>{missingParams.join(', ')}</em> missing.
      </div>
    );
  }

  const validScopes = ['query-unemployment'];

  if (!validScopes.includes(router.query.scope as string)) {
    logger.info({ scope: router.query.scope }, 'Invalid scope');

    return (
      <div data-testid="scopeErrorMessage">
        Error, scope not one of <em>{validScopes.join(', ')}</em>.
      </div>
    );
  }

  return (
    <div className="mx-4">
      <ServiceDetails
        clientId={router.query.clientId as string}
        scope={router.query.scope as string}
        foreignUserId={router.query.state as string}
      />
    </div>
  );
}
