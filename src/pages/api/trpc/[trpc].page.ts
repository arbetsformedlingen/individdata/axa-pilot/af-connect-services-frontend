import { createNextApiHandler } from "@trpc/server/adapters/next";
import { createTRPCContext } from "@app/server/api/trpc";
import { appRouter } from "@app/server/api/root";
import { env } from "@app/env.mjs";
import { type OnErrorFunction } from "@trpc/server/dist/internals/types";
import { type NextApiRequest } from "next/types";

// eslint-disable-next-line  @typescript-eslint/no-explicit-any
export const errorHandler: OnErrorFunction<any, NextApiRequest> = ({ path, error }): void => {
  console.error(
    `❌ tRPC failed on ${path ?? "<no-path>"}: ${error.message}`
  );
}

// export API handler
export const apiHandler = (environment: typeof env) => {
  return createNextApiHandler({
    router: appRouter,
    createContext: createTRPCContext,
    onError:
      environment.NODE_ENV === "development"
        ? errorHandler
        : undefined,
  })
}

export default apiHandler(env);
