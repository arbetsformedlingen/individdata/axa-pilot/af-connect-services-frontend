/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { afterEach, describe, expect, it, vi } from 'vitest';
import { apiHandler, errorHandler } from './[trpc].page';
import { type NextApiRequest } from 'next';

const { mockCreateNextApiHandler } = vi.hoisted(() => ({
  mockCreateNextApiHandler: vi.fn(() => 'Fake API Handler'),
}));

vi.mock('@trpc/server/adapters/next', () => ({
  createNextApiHandler: mockCreateNextApiHandler,
}));

vi.mock('@app/server/api/root', () => ({
  appRouter: 'FAKE APP Router',
}));

vi.mock('@app/server/api/trpc', () => ({
  createTRPCContext: 'FAKE TRPCContext',
}));

const consoleErrorMock = vi.fn()

vi.stubGlobal('console', {
  log: vi.fn(),
  error: consoleErrorMock
})

describe('API Handler', () => {
  afterEach(() => {
    vi.clearAllMocks()
  });

  it('Should be created when in test env', () => {
    const actualApiHandler = apiHandler({ NODE_ENV: 'test', SERVICE_URL: '', AUTH_HEADER: '' });
    expect(actualApiHandler).toBe('Fake API Handler');
    expect(mockCreateNextApiHandler).toBeCalledWith({
      createContext: 'FAKE TRPCContext',
      onError: undefined,
      router: 'FAKE APP Router',
    });
    console.log(mockCreateNextApiHandler.mock.calls);
  });

  it('Should be created when in development env', () => {
    const actualApiHandler = apiHandler({ NODE_ENV: 'development', SERVICE_URL: '', AUTH_HEADER: '' });
    expect(actualApiHandler).toBe('Fake API Handler');
    expect(mockCreateNextApiHandler).toBeCalledWith({
      createContext: 'FAKE TRPCContext',
      onError: expect.any(Function),
      router: 'FAKE APP Router',
    });
    console.log(mockCreateNextApiHandler.mock.calls);
  });
});

describe('errorHandler', () => {
  afterEach(() => {
    vi.clearAllMocks()
  });

  it('should log error message with path present', () => {
    errorHandler({ path: '/some/error/path', error: { message: 'Some nasty error.', code: 'BAD_REQUEST', name: 'TestErrorName' }, type: 'query', req: {} as NextApiRequest, input: {}, ctx: {} })

    expect(consoleErrorMock).toBeCalledWith("❌ tRPC failed on /some/error/path: Some nasty error.")
  })

  it('should log error message without no path present', () => {
    errorHandler({ path: undefined, error: { message: 'Some nasty error.', code: 'BAD_REQUEST', name: 'TestErrorName' }, type: 'query', req: {} as NextApiRequest, input: {}, ctx: {} })

    expect(consoleErrorMock).toBeCalledWith("❌ tRPC failed on <no-path>: Some nasty error.")
  })
});
