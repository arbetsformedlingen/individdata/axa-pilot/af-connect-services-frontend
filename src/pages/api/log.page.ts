import { getLogger } from '@app/utils/logger';
import { type NextApiRequest, type NextApiResponse } from 'next';
import { z } from 'zod';


type ResponseData = {
  message: string
}

export default function handler(req: NextApiRequest, res: NextApiResponse<ResponseData>) {
  const logger = getLogger('frontend');

  if (req.method !== 'POST') {
    res.status(405).json({ message: 'Only POST requests allowed.' })
  }

  const logSchema = z.object({ level: z.string().optional(), msg: z.tuple([z.string()]).or(z.tuple([z.object({}).passthrough(), z.string()])) });
  const logData = logSchema.safeParse(req.body);

  if (!logData.success) {
    return res.status(400).json({ message: 'Wrong format' })
  }

  const { msg, level = 'info' } = logData.data;

  if (msg.length === 1) {
    // Log message without context
    logger.info({ msg: msg[0], level })
  } else {
    // Log message with context
    logger.info({ ...msg[0], level }, msg[1])
  }

  res.status(200).json({ message: 'Log received' })
}
