// @vitest-environment jsdom
import React from 'react';
import { describe, expect, it } from 'vitest';
import { render, screen } from '@testing-library/react';
import { MyApp } from './_app.page';

describe('MyApp', () => {
  it('renders without crashing', () => {
    const DummyComponent: React.FC = () => <p data-testid="dummy">Dummy</p>;

    render(<MyApp Component={DummyComponent} pageProps={{}} router={undefined} />);

    const dummy = screen.queryByTestId('dummy');
    expect(dummy).toBeInTheDocument();
  });
});
