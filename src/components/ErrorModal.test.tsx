// @vitest-environment jsdom
/* eslint-disable @typescript-eslint/no-unsafe-call */

import { describe, expect, it, vi } from 'vitest';
import { render, screen } from '@testing-library/react';

import ErrorModal from './ErrorModal';

describe('ErrorModal', () => {
  it('renders with custom header and message', () => {
    const onCloseMock = vi.fn();

    render(<ErrorModal onClose={onCloseMock} title="some error header" message="some error message" />);

    const errorModal = screen.queryByTestId('errorModal');
    expect(errorModal).toBeInTheDocument();
    expect(errorModal?.getAttribute('af-heading')).toBe('some error header');

    const errorMessage = screen.getByText(/some error message/i);
    expect(errorMessage).toBeInTheDocument();
  });

  it('renders with custom header and no message', () => {
    const onCloseMock = vi.fn();

    render(<ErrorModal onClose={onCloseMock} title="some error header" />);

    const errorModal = screen.queryByTestId('errorModal');
    expect(errorModal).toBeInTheDocument();
    expect(errorModal?.getAttribute('af-heading')).toBe('some error header');

    const errorMessage = screen.getByText(/Vänligen försök igen senare./i);
    expect(errorMessage).toBeInTheDocument();
  });

  it('renders with no header and custom message', () => {
    const onCloseMock = vi.fn();

    render(<ErrorModal onClose={onCloseMock} message="some error message" />);

    const errorModal = screen.queryByTestId('errorModal');
    expect(errorModal).toBeInTheDocument();
    expect(errorModal?.getAttribute('af-heading')).toBe('Något gick fel');

    const errorMessage = screen.getByText(/some error message/i);
    expect(errorMessage).toBeInTheDocument();
  });

  it('renders with neither header nor message', () => {
    const onCloseMock = vi.fn();

    render(<ErrorModal onClose={onCloseMock} />);

    const errorModal = screen.queryByTestId('errorModal');
    expect(errorModal).toBeInTheDocument();
    expect(errorModal?.getAttribute('af-heading')).toBe('Något gick fel');

    const errorMessage = screen.getByText(/Vänligen försök igen senare./i);
    expect(errorMessage).toBeInTheDocument();
  });
});
