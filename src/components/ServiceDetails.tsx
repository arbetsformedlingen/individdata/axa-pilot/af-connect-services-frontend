import { useEffect, useRef, useState } from 'react';
import { createPortal } from 'react-dom';
import { useRouter } from 'next/router';

import { api } from '@app/utils/api';
import {
  DigiButton,
  DigiFormFieldset,
  DigiFormRadiobutton,
  DigiLayoutContainer,
  DigiLink,
  DigiTypography,
  DigiFormValidationMessage,
  DigiFormRadiogroup,
} from '@digi/arbetsformedlingen-react';
import {
  ButtonVariation,
  LinkVariation,
  FormRadiobuttonVariation,
  FormValidationMessageVariation,
} from '@digi/arbetsformedlingen';
import LogoHeader from './LogoHeader';
import Error from './ErrorModal';
import { getLogger } from '@app/utils/logger';

const logger = getLogger('frontend');

type Props = {
  clientId: string;
  scope: string;
  foreignUserId: string;
};

enum ConsentChoice {
  Unknown = 'UNKNOWN',
  Give = 'GIVE',
  Revoke = 'REVOKE',
}

function ServiceDetails({ clientId, scope, foreignUserId }: Props) {
  const [userConsent, setUserConsent] = useState<ConsentChoice>(ConsentChoice.Unknown);
  type ErrorState = {
    show: boolean;
    title?: string;
    message?: string;
  };

  const [error, setError] = useState<ErrorState>({ show: false, title: undefined, message: undefined });
  const [validationError, setValidationError] = useState(false);

  const [targetUrl, setTargetUrl] = useState<string | undefined>(undefined);
  const preventLeaving = useRef(true);


  const router = useRouter();

  const handleBeforeunloadEvent = (event: BeforeUnloadEvent) => {
    if (preventLeaving.current) {
      event.preventDefault();
      event.returnValue = '';
    }
  };

  // Handle event listener for beforeunload events.
  useEffect(() => {
    window.addEventListener('beforeunload', handleBeforeunloadEvent);

    return () => {
      window.removeEventListener('beforeunload', handleBeforeunloadEvent);
    };
  }, []);

  // Push new route when state targetUrl is set.
  useEffect(() => {
    const effect = async () => {
      if (targetUrl) {
        preventLeaving.current = false;
        await router.push(targetUrl);
      }
    }

    effect().catch(logger.error);
  }, [router, targetUrl])

  const serviceInfo = api.service.getInfo.useQuery({ clientId });

  const giveConsent = api.consent.giveConsent.useMutation({
    onSuccess: (data) => {
      if (data.message === 'OK' && data.redirectUrl) {
        setTargetUrl(data.redirectUrl);
      } else if (data.message === 'Not possible at this time') {
        setError({
          show: true,
          title: 'Oops',
          message: 'Vi kunde inte registrera ditt medgivande vid denna tidpunkt. Är du inskriven som arbetssökande?',
        });
        setTargetUrl(undefined);
      }
    },
    onError: (error) => {
      console.log(error);
      setError({ show: true });
      setTargetUrl(undefined);
    },
  });

  const revokeConsent = api.consent.revokeConsent.useMutation({
    onSuccess: (data) => {
      if (data.message === 'OK' && data.redirectUrl) {
        setTargetUrl(data.redirectUrl);
      } else {
        console.log(data);
        setError({ show: true });
        setTargetUrl(undefined);
      }
    },
    onError: (error) => {
      console.log(error);
      setError({ show: true });
      setTargetUrl(undefined);
    },
  });

  const handleSend = (userConsent: ConsentChoice) => {
    if (userConsent === ConsentChoice.Unknown) {
      logger.info({ clientId, scope, foreignUserId }, 'User failed to make selection.');
      setValidationError(true);
    }

    if (userConsent === ConsentChoice.Give) {
      logger.info({ clientId, scope, foreignUserId }, 'User providing consent.');
      giveConsent.mutate({ clientId, scope, foreignUserId });
    }

    if (userConsent === ConsentChoice.Revoke) {
      logger.info({ clientId, scope, foreignUserId }, 'User revoking consent.');
      revokeConsent.mutate({ clientId, scope, foreignUserId });
    }
  };

  return (
    <DigiLayoutContainer data-testid="serviceDetails">
      {error.show &&
        createPortal(
          <Error onClose={() => setError({ show: false })} title={error.title} message={error.message} />,
          document.body,
        )}

      <LogoHeader partnerLogoUrl={serviceInfo.data?.logoUrl} />

      <DigiTypography>
        <DigiLayoutContainer afVerticalPadding afNoGutter>
          <h2>Din data - ditt val</h2>
          <p>
            För att kunna ta ett beslut i ditt försäkringsärende behöver
            <span data-testid="serviceDetailsCompany">{serviceInfo.data?.name ?? '...'}</span> ett intyg att du är
            inskriven hos Arbetsförmedlingen.
          </p>
          <p>
            Intyget som skickas innehåller <em>Inskrivningsdatum</em> och <em>Personnummer</em>.
          </p>
          <div className="pb-6">
            <DigiFormFieldset>
              <DigiFormRadiogroup afName="consent">
                <DigiFormRadiobutton
                  afValue="give"
                  afLabel={`Jag vill automatiskt skicka intyget att jag är inskriven hos Arbetsförmedlingen till ${
                    serviceInfo.data?.name ?? '...'
                  }.`}
                  afVariation={FormRadiobuttonVariation.PRIMARY}
                  onAfOnInput={() => {
                    setValidationError(false);
                    setUserConsent(ConsentChoice.Give);
                  }}
                  data-testid="giveConsentRadioButton"
                />

                <DigiFormRadiobutton
                  afValue="revoke"
                  afLabel={`Jag vill själv hämta intyget hos Arbetsförmedlingen och skicka det till ${
                    serviceInfo.data?.name ?? '...'
                  } varje månad under min pågående arbetslöshet.`}
                  afVariation={FormRadiobuttonVariation.PRIMARY}
                  onAfOnInput={() => {
                    setValidationError(false);
                    setUserConsent(ConsentChoice.Revoke);
                  }}
                  data-testid="revokeConsentRadioButton"
                />
              </DigiFormRadiogroup>
            </DigiFormFieldset>
          </div>
          <p>
            <strong>Intyget kommer att skickas varje månad så länge du är inskriven hos Arbetsförmedlingen.</strong>
          </p>
          <p>
            <strong>
              Du kan avbryta den automatiska överföringen när du vill. Var god observera att det kan påverka din
              försäkringssituation hos {serviceInfo.data?.name ?? '...'}.
            </strong>
          </p>
          <div className="flex flex-col gap-4 md:flex-row">
            <DigiButton
              onAfOnClick={() => handleSend(userConsent)}
              afVariation={ButtonVariation.PRIMARY}
              afFullWidth
              data-testid="sendConsentForm"
            >
              Skicka
            </DigiButton>
            {validationError && (
              <DigiFormValidationMessage
                afVariation={FormValidationMessageVariation.ERROR}
                className="flex flex-row self-center"
                data-testid="formValidationError"
              >
                <p>Vänligen välj ett alternativ för att fortsätta.</p>
              </DigiFormValidationMessage>
            )}
          </div>
          <p className="my-6">
            <DigiLink
              afHref="https://arbetsformedlingen.se/om-webbplatsen/juridisk-information"
              afVariation={LinkVariation.SMALL}
              data-testid="legalInfoLink"
            >
              Så hanterar vi dina personuppgifter
            </DigiLink>
          </p>
        </DigiLayoutContainer>
      </DigiTypography>
    </DigiLayoutContainer>
  );
}

export default ServiceDetails;
