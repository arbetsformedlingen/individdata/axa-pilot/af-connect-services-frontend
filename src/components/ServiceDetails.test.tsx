// @vitest-environment jsdom
/* eslint-disable @typescript-eslint/no-unsafe-call */

import { afterAll, afterEach, beforeAll, describe, expect, it, vi } from 'vitest';
import { fireEvent, screen, waitFor, within } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import ServiceDetails from './ServiceDetails';

import { setupServer } from 'msw/node';
import { trpcMsw } from '../../tests/msw/mockTrpc';
import { renderWithTRPCContext } from '../../tests/msw/mockTrpc';
import { type NextRouter, useRouter } from 'next/router';

vi.mock('next/router', () => ({
  useRouter: vi.fn(),
}));

const server = setupServer(
  trpcMsw.service.getInfo.query((_req, res, ctx) => {
    return res(ctx.status(200), ctx.data({ id: 'acme-001', name: 'ACME Inc.', logoUrl: './some-logo-url.svg' }));
  }),
);

describe('ServiceDetails', () => {
  beforeAll(() => server.listen());

  afterEach(() => server.resetHandlers());

  afterAll(() => server.close());

  it('renders all parts', async () => {
    renderWithTRPCContext(<ServiceDetails clientId="abcd" scope="query-unemployment" foreignUserId="xyzw" />);

    const serviceDetails = screen.queryByTestId('serviceDetails');
    expect(serviceDetails).toBeInTheDocument();

    // Let components from designsystemet hydrate
    await waitFor(() => {
      expect(serviceDetails).toHaveClass('hydrated');
    }, { timeout: 5000 });

    // Company name present
    await waitFor(() => {
      const serviceDetailsCompany = within(serviceDetails!).queryByTestId('serviceDetailsCompany');
      expect(serviceDetailsCompany?.innerHTML).toBe('ACME Inc.');
    });

    // Header component present
    const logoHeader = within(serviceDetails!).queryByTestId('logoHeader');
    expect(logoHeader).toBeInTheDocument();

    const logoWithinHeader = within(serviceDetails!).queryByAltText('Partner logotype');
    expect(logoWithinHeader?.getAttribute('src')).toBe('./some-logo-url.svg');

    // No error shown
    const errorModal = within(serviceDetails!).queryByTestId('errorModal');
    expect(errorModal).not.toBeInTheDocument();

    // Give consent radio button present
    const giveConsentRadioButton = within(serviceDetails!).queryByTestId('giveConsentRadioButton');
    expect(giveConsentRadioButton).toBeInTheDocument();

    // Revoke consent radio button present
    const revokeConsentRadioButton = within(serviceDetails!).queryByTestId('revokeConsentRadioButton');
    expect(revokeConsentRadioButton).toBeInTheDocument();

    // Send button
    const sendConsentForm = within(serviceDetails!).queryByTestId('sendConsentForm');
    expect(sendConsentForm).toBeInTheDocument();

    // No form validation error
    const formValidationError = within(serviceDetails!).queryByTestId('formValidationError');
    expect(formValidationError).not.toBeInTheDocument();

    // Legal info
    const legalInfoLink = within(serviceDetails!).queryByTestId('legalInfoLink');
    expect(legalInfoLink).toBeInTheDocument();
  });

  it('renders validation error when no alternative is choosen', async () => {
    renderWithTRPCContext(<ServiceDetails clientId="abcd" scope="query-unemployment" foreignUserId="xyzw" />);

    const serviceDetails = screen.queryByTestId('serviceDetails');

    // Let components from designsystemet hydrate
    await waitFor(() => {
      expect(serviceDetails).toHaveClass('hydrated');
    });

    const button = within(serviceDetails!).queryByRole('button', { name: /skicka/i });

    fireEvent.click(button!);

    const formValidationError = within(serviceDetails!).queryByTestId('formValidationError');
    expect(formValidationError).toBeInTheDocument();
  });

  it('renders and user can give consent', async () => {
    server.use(
      trpcMsw.consent.giveConsent.mutation((_req, res, ctx) => {
        return res(ctx.status(200), ctx.data({ message: 'OK', redirectUrl: 'https://example.com/redirect/url' }));
      }),
    );

    const pushMock = vi.fn();

    vi.mocked(useRouter).mockReturnValue({
      push: pushMock,
    } as unknown as NextRouter);

    const user = userEvent.setup();

    renderWithTRPCContext(<ServiceDetails clientId="abcd" scope="query-unemployment" foreignUserId="xyzw" />);

    const serviceDetails = screen.queryByTestId('serviceDetails');

    // Let components from designsystemet hydrate
    await waitFor(() => {
      expect(serviceDetails).toHaveClass('hydrated');
    });

    const giveConsent = screen.queryAllByRole('radio').filter((control) => control.getAttribute('value') === 'give')[0];

    await user.click(giveConsent!);

    await user.click(screen.queryByRole('button', { name: /skicka/i })!);

    const formValidationError = within(serviceDetails!).queryByTestId('formValidationError');
    expect(formValidationError).not.toBeInTheDocument();

    // Verify redirect
    await waitFor(() => {
      expect(pushMock).toBeCalledWith('https://example.com/redirect/url');
    })

    // Verify not being prevented to leave the page
    const mockPreventDefault = vi.fn();
    const event = new Event('beforeunload');

    event.preventDefault = mockPreventDefault;

    fireEvent(window, event);

    expect(mockPreventDefault).not.toBeCalled();
  });

  it('renders error message when user consent fails because not in AIS', async () => {
    server.use(
      trpcMsw.consent.giveConsent.mutation((_req, res, ctx) => {
        return res(ctx.status(200), ctx.data({ message: 'Not possible at this time' }));
      }),
    );

    const user = userEvent.setup();

    renderWithTRPCContext(<ServiceDetails clientId="abcd" scope="query-unemployment" foreignUserId="xyzw" />);

    const serviceDetails = screen.queryByTestId('serviceDetails');

    // Let components from designsystemet hydrate
    await waitFor(() => {
      expect(serviceDetails).toHaveClass('hydrated');
    });

    const giveConsent = screen.queryAllByRole('radio').filter((control) => control.getAttribute('value') === 'give')[0];

    await user.click(giveConsent!);

    await user.click(screen.queryByRole('button', { name: /skicka/i })!);

    await waitFor(() => {
      const errorModal = screen.queryByTestId('errorModal');
      expect(errorModal).toBeInTheDocument();
    });
  });

  it('renders error message when tRPC communication fails during consent', async () => {
    server.use(
      trpcMsw.consent.giveConsent.mutation((_req, res, ctx) => {
        return res(ctx.status(500));
      }),
    );

    const user = userEvent.setup();

    renderWithTRPCContext(<ServiceDetails clientId="abcd" scope="query-unemployment" foreignUserId="xyzw" />);

    const serviceDetails = screen.queryByTestId('serviceDetails');

    // Let components from designsystemet hydrate
    await waitFor(() => {
      expect(serviceDetails).toHaveClass('hydrated');
    });

    const giveConsent = screen.queryAllByRole('radio').filter((control) => control.getAttribute('value') === 'give')[0];

    await user.click(giveConsent!);

    await user.click(screen.queryByRole('button', { name: /skicka/i })!);

    await waitFor(() => {
      const errorModal = screen.queryByTestId('errorModal');
      expect(errorModal).toBeInTheDocument();
    });
  });

  it('renders and user can revoke consent', async () => {
    server.use(
      trpcMsw.consent.revokeConsent.mutation((_req, res, ctx) => {
        return res(ctx.status(200), ctx.data({ message: 'OK', redirectUrl: 'https://example.com/redirect/url' }));
      }),
    );
    const pushMock = vi.fn();

    vi.mocked(useRouter).mockReturnValue({
      push: pushMock,
    } as unknown as NextRouter);

    const user = userEvent.setup();

    renderWithTRPCContext(<ServiceDetails clientId="abcd" scope="query-unemployment" foreignUserId="xyzw" />);

    const serviceDetails = screen.queryByTestId('serviceDetails');

    // Let components from designsystemet hydrate
    await waitFor(() => {
      expect(serviceDetails).toHaveClass('hydrated');
    });

    const revokeConsent = screen
      .queryAllByRole('radio')
      .filter((control) => control.getAttribute('value') === 'revoke')[0];

    await user.click(revokeConsent!);

    await user.click(screen.queryByRole('button', { name: /skicka/i })!);

    const formValidationError = within(serviceDetails!).queryByTestId('formValidationError');
    expect(formValidationError).not.toBeInTheDocument();

    // Verify redirect
    await waitFor(() => {
      expect(pushMock).toBeCalledWith('https://example.com/redirect/url');
    })

    // Verify not being prevented to leave the page
    const mockPreventDefault = vi.fn();
    const event = new Event('beforeunload');

    event.preventDefault = mockPreventDefault;

    fireEvent(window, event);

    expect(mockPreventDefault).not.toBeCalled();
  });

  it('renders error message when user consent revokation fails', async () => {
    server.use(
      trpcMsw.consent.revokeConsent.mutation((_req, res, ctx) => {
        return res(ctx.status(200), ctx.data({ message: 'Error' }));
      }),
    );

    const user = userEvent.setup();

    renderWithTRPCContext(<ServiceDetails clientId="abcd" scope="query-unemployment" foreignUserId="xyzw" />);

    const serviceDetails = screen.queryByTestId('serviceDetails');

    // Let components from designsystemet hydrate
    await waitFor(() => {
      expect(serviceDetails).toHaveClass('hydrated');
    });

    const revokeConsent = screen
      .queryAllByRole('radio')
      .filter((control) => control.getAttribute('value') === 'revoke')[0];

    await user.click(revokeConsent!);

    await user.click(screen.queryByRole('button', { name: /skicka/i })!);

    await waitFor(() => {
      const errorModal = screen.queryByTestId('errorModal');
      expect(errorModal).toBeInTheDocument();
    });
  });

  it('renders error message when tRPC communication fails during consent revokation', async () => {
    server.use(
      trpcMsw.consent.revokeConsent.mutation((_req, res, ctx) => {
        return res(ctx.status(500));
      }),
    );

    const user = userEvent.setup();

    renderWithTRPCContext(<ServiceDetails clientId="abcd" scope="query-unemployment" foreignUserId="xyzw" />);

    const serviceDetails = screen.queryByTestId('serviceDetails');

    // Let components from designsystemet hydrate
    await waitFor(() => {
      expect(serviceDetails).toHaveClass('hydrated');
    });

    const revokeConsent = screen
      .queryAllByRole('radio')
      .filter((control) => control.getAttribute('value') === 'revoke')[0];

    await user.click(revokeConsent!);

    await user.click(screen.queryByRole('button', { name: /skicka/i })!);

    await waitFor(() => {
      const errorModal = screen.queryByTestId('errorModal');
      expect(errorModal).toBeInTheDocument();
    });
  });

  it('tries to prevent the user from leaving', async () => {
    renderWithTRPCContext(<ServiceDetails clientId="abcd" scope="query-unemployment" foreignUserId="xyzw" />);

    const serviceDetails = screen.queryByTestId('serviceDetails');
    expect(serviceDetails).toBeInTheDocument();

    // Let components from designsystemet hydrate
    await waitFor(() => {
      expect(serviceDetails).toHaveClass('hydrated');
    });

    const mockPreventDefault = vi.fn();
    const event = new Event('beforeunload');

    event.preventDefault = mockPreventDefault;

    fireEvent(window, event);

    expect(mockPreventDefault).toBeCalled();
  });
});
