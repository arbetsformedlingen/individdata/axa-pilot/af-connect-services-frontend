import React from 'react';

import type { Meta, StoryFn } from '@storybook/react';
import LogoHeader from './LogoHeader';
import { DigiLayoutContainer } from '@digi/arbetsformedlingen-react';

export default {
  title: 'Components/Header',
  component: LogoHeader,
  args: {
    partnerLogoUrl: './abc-000001.svg',
  },
  parameters: {
    layout: 'fullscreen',
  },
} as Meta<typeof LogoHeader>;

const Template: StoryFn<typeof LogoHeader> = (args) => (
  <DigiLayoutContainer>
    <LogoHeader {...args} />
  </DigiLayoutContainer>
);

export const Default = Template.bind({});
