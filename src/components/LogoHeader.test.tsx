// @vitest-environment jsdom
/* eslint-disable @typescript-eslint/no-unsafe-call */

import { describe, expect, it } from 'vitest';
import { render, screen, within } from '@testing-library/react';

import LogoHeader from './LogoHeader';

describe('LogoHeader', () => {
  it('renders with partner URL', () => {
    render(<LogoHeader partnerLogoUrl="./dummy_url.svg" />);

    const logoHeader = screen.queryByTestId('logoHeader');
    expect(logoHeader).toBeInTheDocument();

    const afLogo = within(logoHeader!).queryByTestId('afLogo');
    expect(afLogo).toBeInTheDocument();

    const partnerLogo = within(logoHeader!).queryByTestId('partnerLogoURL');
    expect(partnerLogo).toBeInTheDocument();
    expect(partnerLogo?.getAttribute('src')).toBe('./dummy_url.svg');
  });

  it('renders without partner URL', () => {
    render(<LogoHeader partnerLogoUrl={undefined} />);

    const logoHeader = screen.queryByTestId('logoHeader');
    expect(logoHeader).toBeInTheDocument();

    const afLogo = within(logoHeader!).queryByTestId('afLogo');
    expect(afLogo).toBeInTheDocument();

    const partnerLogo = within(logoHeader!).queryByTestId('partnerLogoURL');
    expect(partnerLogo).not.toBeInTheDocument();
  });
});
