import React from 'react';

import type { Meta, StoryFn } from '@storybook/react';
import ServiceDetails from './ServiceDetails';

import {
  giveConsentHandler,
  giveConsentHandlerError,
  revokeConsentHandler,
  revokeConsentHandlerError,
} from 'tests/msw/handlers/consent';
import { getInfo } from 'tests/msw/handlers/service';

export default {
  title: 'Components/ServiceDetails',
  component: ServiceDetails,
  args: {
    clientId: 'abcd',
    scope: 'query-unemployment',
    foreignUserId: 'efgh',
  },
  parameters: {
    msw: {
      handlers: [giveConsentHandler(), revokeConsentHandler(), getInfo()],
    },
    layout: 'fullscreen',
  },
} as Meta<typeof ServiceDetails>;

const Template: StoryFn<typeof ServiceDetails> = (args) => <ServiceDetails {...args} />;

export const Default = Template.bind({});

export const Error = Template.bind({});
Error.parameters = {
  msw: {
    handlers: [giveConsentHandlerError(), revokeConsentHandlerError(), getInfo()],
  },
};
