import { DialogSize } from '@digi/arbetsformedlingen';
import { DigiDialog } from '@digi/arbetsformedlingen-react';

type Props = { onClose: () => void; title?: string; message?: string };

const ErrorModal = ({ onClose, title, message }: Props) => {
  return (
    <DigiDialog
      onAfOnClose={onClose}
      afSize={DialogSize.SMALL}
      afShowDialog={true}
      afHeading={title ?? 'Något gick fel'}
      data-testid="errorModal"
    >
      {message ?? 'Vänligen försök igen senare.'}
    </DigiDialog>
  );
};

export default ErrorModal;
