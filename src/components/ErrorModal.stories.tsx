import React from 'react';

import type { Meta, StoryFn } from '@storybook/react';
import ErrorModal from './ErrorModal';

export default {
  title: 'Components/ErrorModal',
  component: ErrorModal,
} as Meta<typeof ErrorModal>;

const Template: StoryFn<typeof ErrorModal> = (args) => <ErrorModal {...args} />;

export const Default = Template.bind({});
