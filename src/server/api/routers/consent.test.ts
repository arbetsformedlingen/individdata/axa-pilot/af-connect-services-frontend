/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import { afterAll, beforeAll, afterEach, describe, expect, it, vi } from 'vitest';
import { appRouter } from '../root';
import { createInnerTRPCContext } from '../trpc';
import { buildHeaders } from './consent';

describe('getHeaders', () => {
  it('returns correct headers', () => {
    const headers = buildHeaders('199001019876');

    expect(headers['Content-Type']).toBe('application/json');
    expect(headers.PISA_ID).toBe('199001019876');
  });
});

describe('consent router', () => {
  const mockFetch = vi.fn();

  beforeAll(() => {
    vi.stubGlobal('fetch', mockFetch);
  });

  afterEach(() => {
    mockFetch.mockReset();
  });

  afterAll(() => {
    vi.unstubAllGlobals();
  });

  describe('giveConsent', () => {
    it('should call backend and return with redirect URL', async () => {
      mockFetch.mockResolvedValue({
        ok: true,
        json: vi.fn(() => ({
          authCode: 'abcd',
          clientId: '',
          enabled: true,
          name: 'Some name',
          redirectUrl: 'http://example.com/return',
        })),
      });

      const ctx = createInnerTRPCContext({ user: '199909091234' });
      const caller = appRouter.createCaller(ctx);

      const input = {
        clientId: 'abcd',
        scope: 'query-unemployment',
        foreignUserId: 'efgh',
      };

      const request = await caller.consent.giveConsent(input);

      expect(mockFetch).toBeCalledWith(
        'http://localhost:4000/user/giveConsent2?clientId=abcd&scope=query-unemployment&axaId=efgh',
        {
          headers: {
            'Content-Type': 'application/json',
            'PISA_ID': '199909091234'
          }
        }
      );

      expect(request.message).toBe('OK');
      expect(request.redirectUrl).toBe('http://example.com/return?authCode=abcd&state=efgh&message=OK');
    });

    it('should call backend and handle unregistered user', async () => {
      mockFetch.mockResolvedValue({
        ok: false,
        json: vi.fn(() => ({
          message: 'Person not registered in AIS'
        })),
        status: 404,
      });

      const ctx = createInnerTRPCContext({ user: '199909091234' });
      const caller = appRouter.createCaller(ctx);

      const input = {
        clientId: 'abcd',
        scope: 'query-unemployment',
        foreignUserId: 'efgh',
      };

      const request = await caller.consent.giveConsent(input);

      expect(mockFetch).toBeCalledWith(
        "http://localhost:4000/user/giveConsent2?clientId=abcd&scope=query-unemployment&axaId=efgh",
        {
          headers: {
            "Content-Type": "application/json",
            "PISA_ID": "199909091234",
          }
        })

      expect(request.message).toBe('Not possible at this time');
    })

    it('should call backend and handle error message', async () => {
      mockFetch.mockResolvedValue({
        ok: false,
        json: vi.fn(),
        status: 500,
        statusText: 'Internal server error.'
      });

      const ctx = createInnerTRPCContext({ user: '199909091234' });
      const caller = appRouter.createCaller(ctx);

      const input = {
        clientId: 'abcd',
        scope: 'query-unemployment',
        foreignUserId: 'efgh',
      };

      const request = await caller.consent.giveConsent(input);

      expect(mockFetch).toBeCalledTimes(1);
      expect(mockFetch.mock.calls[0][0]).toBe(
        'http://localhost:4000/user/giveConsent2?clientId=abcd&scope=query-unemployment&axaId=efgh',
      );
      expect(mockFetch.mock.calls[0][1].headers.PISA_ID).toBe('199909091234');

      expect(request.message).toBe('Failed to provide consent, invalid response from back end.');
    });
  })

  describe('revokeConsent', () => {
    it('should call backend and return with redirect URL', async () => {
      mockFetch.mockResolvedValue({
        ok: true,
        json: vi.fn(() => ({
          redirectUrl: 'http://example.com/return',
          authCode: null,
        })),
      });

      const ctx = createInnerTRPCContext({ user: '199909091234' });
      const caller = appRouter.createCaller(ctx);

      const input = {
        clientId: 'abcd',
        scope: 'query-unemployment',
        foreignUserId: 'efgh',
      };

      const request = await caller.consent.revokeConsent(input);



      expect(mockFetch).toHaveBeenCalledWith(
        'http://localhost:4000/user/revokeConsent2?clientId=abcd&scope=query-unemployment&axaId=efgh',
        {
          headers: {
            'Content-Type': 'application/json',
            'PISA_ID': '199909091234'
          }
        }
      );

      expect(request.message).toBe('OK');
      expect(request.redirectUrl).toBe('http://example.com/return?authCode=&state=efgh&message=Error');
    });

    it('should call backend and handle error message', async () => {
      mockFetch.mockResolvedValue({
        ok: false,
        json: vi.fn(),
        status: 500,
        statusText: 'Internal server error.'
      });

      const ctx = createInnerTRPCContext({ user: '199909091234' });
      const caller = appRouter.createCaller(ctx);

      const input = {
        clientId: 'abcd',
        scope: 'query-unemployment',
        foreignUserId: 'efgh',
      };

      const request = await caller.consent.revokeConsent(input);

      expect(mockFetch).toBeCalledTimes(1);
      expect(mockFetch.mock.calls[0][0]).toBe(
        'http://localhost:4000/user/revokeConsent2?clientId=abcd&scope=query-unemployment&axaId=efgh',
      );
      expect(mockFetch.mock.calls[0][1].headers.PISA_ID).toBe('199909091234');

      expect(request.message).toBe('Failed to revoke consent, invalid response from back end.');
    });
  });
});
