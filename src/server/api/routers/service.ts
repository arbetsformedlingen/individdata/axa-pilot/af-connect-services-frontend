import { z } from 'zod';
import { env } from '@app/env.mjs';
import { createTRPCRouter, publicProcedure } from '@app/server/api/trpc';
import { getLogger } from '@app/utils/logger';

const logger = getLogger('backend');

export const serviceRouter = createTRPCRouter({
  getInfo: publicProcedure
    .input(z.object({ clientId: z.string() }))
    .query(async ({ input }) => {
      const url = `${env.SERVICE_URL}/service/${input.clientId}/`;
      logger.debug({ url }, 'Fetch service details.');
      const response = await fetch(url);

      if (response.ok) {
        const responseSchema = z.object({ clientId: z.string(), name: z.string() });
        const data = responseSchema.parse(await response.json());

        return {
          id: data.clientId,
          name: data.name,
          logoUrl: `./${data.clientId}.svg`
        }
      } else {
        const message = 'Failed to fetch client details.';
        logger.error({ url, status: response.status, statusText: response.statusText }, message)
        return { message }
      }
    })
})