import { env } from "@app/env.mjs";
import logLevelData from "../../logging.config";
import pino from 'pino';

const logLevelsMap = new Map<string, string>(Object.entries(logLevelData));

export const getLogLevel = (logger: string, logLevels = logLevelsMap) => {
  return logLevels.get(logger) ?? logLevels.get('*') ?? "info";
}

export const getLogger = (name: string, opts?: { redact: string[] }) => {
  let pinoOpts;

  if (typeof window === 'undefined') {
    // Configuration for server side
    pinoOpts = {
      ...opts,
      name,
      level: getLogLevel(name),
      enabled: env.NODE_ENV !== 'test',
    }
  } else {
    // Configuration for client side
    pinoOpts = {
      ...opts,
      name,
      level: getLogLevel(name),
      enabled: true,
      browser: {
        transmit: {
          send: send
        }
      }
    }
  }

  return pino(pinoOpts)
}

export const send = (level: pino.Level, logEvent: pino.LogEvent) => {
  const msg = logEvent.messages;

  const blob = new Blob([JSON.stringify({ msg, level })], { type: 'application/json' });
  navigator.sendBeacon('/api/log', blob);
}