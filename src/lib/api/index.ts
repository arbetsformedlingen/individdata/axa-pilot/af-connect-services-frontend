import createClient from 'openapi-fetch';
import type { paths } from './v2';
import { env } from '@app/env.mjs';

const client = createClient<paths>({ baseUrl: env.SERVICE_URL });

export default client;
