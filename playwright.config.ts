import { defineConfig } from '@playwright/test';

export default defineConfig({
  testDir: 'tests',
  testMatch: '**/*.spec.ts',
  use: {
    baseURL: "http://rproxy:4000",
  },
  // Do not run in parallell on GitLab CI.
  workers: process.env.CI ? 1 : undefined,
})